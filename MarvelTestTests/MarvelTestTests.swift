//
//  MarvelTestTests.swift
//  MarvelTestTests
//
//  Created by Alejandro López Rocha on 05/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import XCTest
import CommonCrypto
@testable import MarvelTest

class MarvelTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
    }
    
    func testGetAllHeroes() {
        
        let responseFromServer: [[String: Any]] =
        [
            [
                "id" : 1,
                "name" : "Heroe 1",
                "description" : "I am The Heroe 1",
                "thumbnail" : [
                    "path": "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0",
                    "extension": "jpg"
                ]
            ],
            [
                "id" : 2,
                "name" : "Heroe 2",
                "description" : "I am The Heroe 2",
                "thumbnail" : [
                    "path": "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0",
                    "extension": "jpg"
                ]
            ],
            [
                "id" : 3,
                "name" : "Heroe 3",
                "description" : "I am The Heroe 3",
                "thumbnail" : [
                    "path": "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0",
                    "extension": "jpg"
                ]
            ],
            [
                "id" : 4,
                "name" : "Heroe 4",
                "description" : "I am The Heroe 4",
                "thumbnail" : [
                    "path": "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0",
                    "extension": "jpg"
                ]
            ],
            [
                "id" : 5,
                "name" : "Heroe 5",
                "description" : "I am The Heroe 5",
                "thumbnail" : [
                    "path": "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0",
                    "extension": "jpg"
                ]
            ],
            
        ]
        let worker = SuperHeroesWorker()
        worker.loadSuperHeroes(endpoint: "", params: "", success: { (resp) in
            if let jsonData = try? JSONSerialization.data(withJSONObject: responseFromServer, options: []) {
                let result = try! JSONDecoder().decode([SuperHeroe].self, from: jsonData)
            XCTAssertGreaterThan(result.count, 0)
            }
        }) { (error) in
            XCTAssertNotNil(error.localizedDescription)
        }
    }
    
    func SearchByName() {
    
    }
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
