//
//  SuperHeroDetailsData.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 09/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//


struct SuperHeroDetailsData: Decodable {
    var comics: [DetailsData]
    var events: [DetailsData]
    var stories: [DetailsData]
    var series: [DetailsData]

}

struct DetailsData: Codable {
    var title: String?
    var description: String?
}


