//
//  SuperHeroDetailsCell.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 09/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

//
//  SuperHeroCell.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 08/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class SuperHeroDetailsCell: UITableViewCell {
    
    fileprivate struct ViewTraits {
        
        static let margin: CGFloat = 8.0
        static let titleTextMargin: CGFloat = 4.0
        static let titleFont: CGFloat = 16.0
        static let descriptionFont: CGFloat = 12.0
        
    }
    
    let name: UILabel
    let descriptionText: UILabel
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        name = UILabel()
        descriptionText = UILabel()
   
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open class func reuseIdentifier() -> String {
        return "common.cell.superHeroeDetails"
    }
    
    private func removeAllComponents() {
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
    }
    
    private func setupComponents() {
        
        self.isUserInteractionEnabled = false
        
        name.textColor = Color.black
        name.font = UIFont.systemFont(ofSize: ViewTraits.titleFont)
        name.numberOfLines = 0
        
        descriptionText.textColor = Color.grey
        descriptionText.font = UIFont.systemFont(ofSize: ViewTraits.descriptionFont)
        descriptionText.numberOfLines = 0
        
        //Add Suviews
        addSubview(name)
        addSubview(descriptionText)

        // Add Constrains
        name.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.translatesAutoresizingMaskIntoConstraints = false

    }
    
    fileprivate func addCustomConstrains() {
        
        NSLayoutConstraint.activate([
            
            //Horizontal
            name.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.margin),
            name.trailingAnchor.constraint(equalTo:trailingAnchor, constant: -ViewTraits.margin),
            descriptionText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.margin),
            descriptionText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -ViewTraits.margin),
            
            //Vertical
            name.topAnchor.constraint(equalTo: topAnchor,constant: ViewTraits.margin),
            descriptionText.topAnchor.constraint(equalTo: name.bottomAnchor, constant: ViewTraits.margin/2),
            ])
    }
    
    func configure(title: String?, description: String?) {
        removeAllComponents()
        setupComponents()
        
        name.text = title
        descriptionText.text = description
        
        addCustomConstrains()
    }
}

