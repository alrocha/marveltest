//
//  SuperHeroDetailsHeader.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 10/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class SuperHeroDetailsHeader: UIView {
    
    private struct ViewTraits {
        static let tableViewSeparatorInset: CGFloat = 10
        static let margin: CGFloat = 15.0
        static let fontNameSize: CGFloat = 25.0
        static let fontDescription: CGFloat = 12.0
        
    }
    
    let name: UILabel
    let image: UIImageView
    let descriptionText: UILabel
    let favoriteIcon: UIImageView
    
    override init(frame: CGRect) {
        
        name = UILabel()
        image = UIImageView()
        descriptionText = UILabel()
        favoriteIcon = UIImageView()
        
        super.init(frame: frame)
        
        setupComponents()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupComponents() {
        
        backgroundColor = .white
        
        name.font = UIFont.boldSystemFont(ofSize: ViewTraits.fontNameSize)
        name.textColor = Color.black
        
        descriptionText.font = UIFont.systemFont(ofSize: ViewTraits.fontDescription)
        descriptionText.numberOfLines = 0
        
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        
        favoriteIcon.isUserInteractionEnabled = true
        favoriteIcon.layoutMargins = UIEdgeInsetsMake(10, 10, 10, 10)

        name.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        image.translatesAutoresizingMaskIntoConstraints = false
        favoriteIcon.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(name)
        addSubview(descriptionText)
        addSubview(image)
        addSubview(favoriteIcon)
    }
    fileprivate func setupConstraints() {
        
        NSLayoutConstraint.activate([
            
            // Horizontal
            name.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.margin),
            image.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.margin),
            descriptionText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.margin),
            descriptionText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -ViewTraits.margin),

            favoriteIcon.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: ViewTraits.margin),
            
            
            // Vertical
            name.topAnchor.constraint(equalTo: topAnchor, constant: ViewTraits.margin),
            image.topAnchor.constraint(equalTo: name.bottomAnchor, constant: ViewTraits.margin),
            descriptionText.topAnchor.constraint(equalTo: image.bottomAnchor, constant: ViewTraits.margin),

            favoriteIcon.bottomAnchor.constraint(equalTo: image.bottomAnchor),
            
            //Size
            
            image.widthAnchor.constraint(equalToConstant: 150.0),
            image.heightAnchor.constraint(equalToConstant: 150.0)
            ])
    }
}
