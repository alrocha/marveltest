//
//  SuperHeroDetailsTableView.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 09/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class SuperHeroDetailsTableView: UIView {
    
    private struct ViewTraits {
        static let tableViewSeparatorInset: CGFloat = 10
        static let margin: CGFloat = 15.0
        static let fontNameSize: CGFloat = 25.0
        static let fontDescription: CGFloat = 12.0
        
    }
    
    let tableView: UITableView
    
    override init(frame: CGRect) {

        tableView = UITableView(frame: frame, style: .plain)

        
        super.init(frame: frame)
        
        setupComponents()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupComponents() {
        
        backgroundColor = .white

        tableView.backgroundColor = .white
        tableView.separatorColor = Color.grey
        tableView.separatorInset.left = ViewTraits.tableViewSeparatorInset
        tableView.separatorInset.right = ViewTraits.tableViewSeparatorInset

        tableView.translatesAutoresizingMaskIntoConstraints = false
 

        addSubview(tableView)

    }
    
    fileprivate func setupConstraints() {
        
        NSLayoutConstraint.activate([
            
            // Horizontal

            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            
            // Vertical

            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            ])
    }
}

