//
//  LoadingViewLogic.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 09/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

protocol LoadingViewLogic where Self: UIViewController {
    
    var loadingViewTag: Int { get }
   
}

extension LoadingViewLogic {
    
    var loadingViewTag: Int {
        return 999
    }
    
    func showLoadingView() {
        if isLoadingViewIsDisplayed() {
            return
        }
        
        let loadingView = LoadingView()
        loadingView.tag = loadingViewTag
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        if let container = getContainerView() {
            container.addSubview(loadingView)
            
                let navigationBarHeight = (navigationController?.navigationBar.frame.height ?? 0) + UIApplication.shared.statusBarFrame.height
                let tabBarHeight = (tabBarController?.tabBar.frame.height ?? 0)
            
            NSLayoutConstraint.activate([
                loadingView.topAnchor.constraint(equalTo: container.topAnchor, constant: navigationBarHeight),
                loadingView.leadingAnchor.constraint(equalTo: container.leadingAnchor),
                loadingView.trailingAnchor.constraint(equalTo: container.trailingAnchor),
                loadingView.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -tabBarHeight)
                ])
        }
    }
    
    func hideLoadingView() {
        guard let loadingView = getCurrentLoadingView() else {
            return
        }
        DispatchQueue.main.async {
            loadingView.removeFromSuperview()
        }
    }
    
    func isLoadingViewIsDisplayed() -> Bool {
        return getCurrentLoadingView() != nil
    }
    
    private func getCurrentLoadingView() -> LoadingView? {
        return getContainerView()?.viewWithTag(loadingViewTag) as? LoadingView
    }
    
    
    private func getContainerView() -> UIView? {
        return UIApplication.shared.keyWindow
    }
}
