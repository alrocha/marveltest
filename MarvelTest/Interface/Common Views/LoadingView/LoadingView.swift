//
//  LoadingView.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 09/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit


class LoadingView: UIView {
    
    fileprivate struct ViewTraits {
        static let loadingViewWidthHeight: CGFloat = 80.0
        static let spinnerWidthHeight: CGFloat = 40.0
    }
    
    let overlayView: UIView
    let loadingSquare: UIView
    let indicator: UIActivityIndicatorView
    
    override init(frame: CGRect) {
        overlayView = UIView()
        loadingSquare = UIView()
        indicator = UIActivityIndicatorView()
        
        super.init(frame: frame)
        
        setupComponents()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupComponents() {
        
        overlayView.backgroundColor = Color.black
        overlayView.alpha = 0.3
        
        
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        indicator.startAnimating()
        
        
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        loadingSquare.translatesAutoresizingMaskIntoConstraints = false
        indicator.translatesAutoresizingMaskIntoConstraints = false

        addSubview(overlayView)
        overlayView.addSubview(loadingSquare)
        loadingSquare.addSubview(indicator)
    }
    
    private func setupConstraints() {
 
        NSLayoutConstraint.activate([
            overlayView.leadingAnchor.constraint(equalTo: leadingAnchor),
            overlayView.trailingAnchor.constraint(equalTo: trailingAnchor),
            overlayView.topAnchor.constraint(equalTo: topAnchor),
            overlayView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            loadingSquare.widthAnchor.constraint(equalToConstant: ViewTraits.loadingViewWidthHeight),
            loadingSquare.heightAnchor.constraint(equalToConstant: ViewTraits.loadingViewWidthHeight),
            loadingSquare.centerXAnchor.constraint(equalTo: centerXAnchor),
            loadingSquare.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            indicator.widthAnchor.constraint(equalToConstant: ViewTraits.spinnerWidthHeight),
            indicator.heightAnchor.constraint(equalToConstant: ViewTraits.spinnerWidthHeight),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor)
            
            ])
        }
}

