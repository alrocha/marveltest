//
//  SuperHeroCell.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 08/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class SuperHeroCell: UITableViewCell {
    
    fileprivate struct ViewTraits {
        
        static let margin: CGFloat = 8.0

        static let titleFont: CGFloat = 14.0
        static let imageWidthHeight: CGFloat = 75.0
        static let horizontalInset: CGFloat = 10.0
        static let verticalInset: CGFloat = 20.0

    }
    
    let titleLabel: UILabel
    let picture: UIImageView
    let arrow: UIImageView
    let favoriteIcon: UIImageView
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        titleLabel = UILabel()
        picture = UIImageView()
        arrow = UIImageView()
        favoriteIcon = UIImageView()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open class func reuseIdentifier() -> String {
        return "common.cell.superHeroe"
    }
    
    private func removeAllComponents() {
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
    }
    
    private func setupComponents() {
        
        self.isExclusiveTouch = true

        titleLabel.textColor = Color.black
        titleLabel.numberOfLines = 0
        
        arrow.image = UIImage(named: "arrow_right")
        
        picture.layer.cornerRadius = 15
        picture.clipsToBounds = true
        
        //Add Suviews
        addSubview(titleLabel)
        addSubview(picture)
        addSubview(arrow)
        addSubview(favoriteIcon)
        
        // Add Constrains
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        picture.translatesAutoresizingMaskIntoConstraints = false
        arrow.translatesAutoresizingMaskIntoConstraints = false
        favoriteIcon.translatesAutoresizingMaskIntoConstraints = false
    } 
    
    fileprivate func addCustomConstrains() {
        
        NSLayoutConstraint.activate([
            
            picture.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.margin),
            picture.topAnchor.constraint(equalTo: topAnchor, constant: ViewTraits.margin),
            picture.widthAnchor.constraint(equalToConstant: ViewTraits.imageWidthHeight),
            picture.heightAnchor.constraint(equalToConstant: ViewTraits.imageWidthHeight),
            
            titleLabel.leadingAnchor.constraint(equalTo: picture.trailingAnchor, constant: ViewTraits.margin),
            titleLabel.trailingAnchor.constraint(equalTo: arrow.leadingAnchor, constant: -ViewTraits.margin),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: ViewTraits.margin),
            
            favoriteIcon.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            favoriteIcon.bottomAnchor.constraint(equalTo: picture.bottomAnchor),

            arrow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -ViewTraits.margin * 2),
            arrow.centerYAnchor.constraint(equalTo: centerYAnchor)
            
            ])
    }
    
    func configure(title: String, desc: String, imageLink: String, favorite: String) {
        removeAllComponents()
        setupComponents()
        
        titleLabel.text = title
        picture.downloadedFrom(link: imageLink)
        favoriteIcon.image = UIImage(named: favorite)
        
        addCustomConstrains()
    }
}
