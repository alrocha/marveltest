//
//  SuperHeroesView.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 08/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class SuperHeroesView: UIView {

private struct ViewTraits {
    static let tableViewSeparatorInsetLeft: CGFloat = 10
    static let tableViewSeparatorInsetRight: CGFloat = 25
    
}

let tableView: UITableView

override init(frame: CGRect) {
    tableView = UITableView(frame: frame, style: .plain)
    
    super.init(frame: frame)
    
    setupComponents()
    setupConstraints()
}

required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}

fileprivate func setupComponents() {
    
    backgroundColor = .white
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = .white
    tableView.separatorColor = Color.grey
    tableView.separatorInset.left = ViewTraits.tableViewSeparatorInsetLeft
    tableView.separatorInset.right = ViewTraits.tableViewSeparatorInsetRight
    tableView.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(tableView)
}

fileprivate func setupConstraints() {
    
    NSLayoutConstraint.activate([
        // Horizontal
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
        
        // Vertical
        tableView.topAnchor.constraint(equalTo: topAnchor),
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}


