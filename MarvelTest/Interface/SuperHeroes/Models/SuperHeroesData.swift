//
//  SuperHeroesData.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 06/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//


struct SuperHeroe: Codable {
    var id: Int
    var name: String
    var description: String
    var modified: String
    var thumbnail: Thumbnail
    
}

struct Thumbnail: Codable {
    var path: String
    var `extension`: String
}
