//
//  SlideUpAnimation.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 10/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class SlideUpAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    private let originFrame: CGRect
    
    init(originFrame: CGRect) {
        self.originFrame = originFrame
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.0
    }
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

    }
    

    
}
