//
//  RequestModel.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 06/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//


struct Pagination: Decodable {
    var offset: Int
    var limit: Int
    var total: Int
    var count: Int
}

