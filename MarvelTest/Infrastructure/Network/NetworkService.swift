//
//  NetworkService.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 06/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import Foundation
import SystemConfiguration

typealias NetworkManagerSuccessHandler = (_ responseObject: Any?) -> ()
typealias NetworkManagerErrorHandler = (_ error: Error) -> ()


struct NetworkService {
    
    static let shared = NetworkService()
    
    internal func request(endPoint: String, params: String, success: @escaping NetworkManagerSuccessHandler, failure: @escaping NetworkManagerErrorHandler)  {
        let baseURL = ApiConfig.ApiURL.urlString
        let urlString = "\(baseURL)\(endPoint)?\(params)ts=\(ApiConfig.APIAuthKeys.timestamp)&apikey=\(ApiConfig.APIAuthKeys.publicKey)&hash=\(ApiConfig.APIAuthKeys.hash)"
        
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (dataResponse, response, error) in
                if let resp = response as? HTTPURLResponse {
                    if resp.statusCode == 200 {
                        if let dict = dataResponse {
                            let json = try? JSONSerialization.jsonObject(with: dict, options: []) as? [String : Any]
                            if let jsonDict = json {
                                if let allData = jsonDict!["data"] {
                                    success(allData)
                                }
                            }
                        }
                    }
                }
                if let err = error {
                    failure(err.localizedDescription as! Error)
                }
            })
            task.resume()
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }


    
    func isInternetReachable() -> Bool
    {
        var sockAddress = sockaddr_in()
        sockAddress.sin_len = UInt8(MemoryLayout.size(ofValue: sockAddress))
        sockAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &sockAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {sockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, sockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}


