//
//  ApiConfig.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 05/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import Foundation

struct ApiConfig {
    
    //MARK: Api Marvel URLs:
    
    internal struct ApiURL {
        static let baseUrl = "gateway.marvel.com/v1/public/"
        static let urlString = "https://\(baseUrl)"
    }
    
    //MARK: Api Keys
    
    internal struct APIAuthKeys {
        
        static let timestamp = String().getCurrentTimeStamp()
        static let publicKey = "0510890aa448b72bab62ee0d2dca11e2"
        static let privateKey = "655d168c890e060777b0320790acf7c5a24a75f9"
        static let hash = String().generateMD5(timestamp: APIAuthKeys.timestamp, privateKey: APIAuthKeys.privateKey, publicKey: APIAuthKeys.publicKey)
    
    }
}
