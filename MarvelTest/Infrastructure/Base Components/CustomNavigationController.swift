//
//  CustomNavigationController.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 08/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let navigationBarAppearence = UINavigationBar.appearance()
        navigationBarAppearence.prefersLargeTitles = true
        navigationBarAppearence.barTintColor = Color.white
        navigationBarAppearence.isTranslucent = false
        
        navigationBarAppearence.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Color.black]
        
        navigationBarAppearence.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: Color.black]
        navigationBarAppearence.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Color.black]
        let backImage = UIImage(named: "back_image")?.withRenderingMode(.alwaysOriginal)
        navigationBarAppearence.backIndicatorImage = backImage
    }
    
}
