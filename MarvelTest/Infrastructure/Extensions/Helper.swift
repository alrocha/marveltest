//
//  Helper.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 06/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit
import CommonCrypto

extension String {
    
    func MD5(string: String) -> Data {
        let keysAndTs = string.data(using:.utf8)!
        var md5Hash = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = md5Hash.withUnsafeMutableBytes {digestBytes in
            keysAndTs.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(keysAndTs.count), digestBytes)
            }
        }
        
        return md5Hash
    }
    func generateMD5(timestamp: String, privateKey: String, publicKey: String) -> String {
        let hash = MD5(string:"\(timestamp)\(privateKey)\(publicKey)")
        let hashString = hash.map { String(format: "%02hhx", $0) }.joined()
        return hashString
    }
    
    
    //MARK: Timestamp
    
    func getCurrentTimeStamp() -> String {
        let dateFormat = DateFormatter()
        let today = Date()
        dateFormat.timeZone = TimeZone(abbreviation: "GMT")
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        let strTime: String = dateFormat.string(from:today  as Date)
        let dateGMT: NSDate = dateFormat.date(from: strTime)! as NSDate
        let milliseconds: String = String(dateGMT.timeIntervalSince1970)
        return milliseconds
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.height
    }
}
    // MARK: Dictionary
extension Dictionary {
    
    mutating func merge<K, V>(_ dict: [K: V]) {
        
        for (k, v) in dict {
            if let v = v as? Value,
                let k = k as? Key {
                
                updateValue(v, forKey: k)
            }
        }
    }
}

extension UIColor {
    
    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}


