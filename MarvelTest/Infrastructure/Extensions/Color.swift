//
//  Color.swift
//  MarvelTest
//
//  Created by Alejandro López Rocha on 08/04/2018.
//  Copyright © 2018 Alejandro López Rocha. All rights reserved.
//

import UIKit

typealias Color = UIColor

enum HexColor: UInt32 {
    case black = 0x20201e
    case dust = 0xf0f0f0
    case green = 0x229932
    case grey = 0x848483
    case greyLight = 0xededed
    case red = 0xdd0000
    case silver = 0xc4c4c3
    case yellow = 0xf8e71c
    case yellowDark = 0xfbe200
    case blueDefaultIOS = 0x007aff
    case blueDark = 0x627BD9
    case blue = 0x61A3D8
    case greenAlpha = 0xe0e9e1
    case redAlpha = 0xf9eced
    
}

extension UIColor {
    
    static let black = UIColor(hexColor: .black)
    static let dust = UIColor(hexColor: .dust)
    static let green = UIColor(hexColor: .green)
    static let grey = UIColor(hexColor: .grey)
    static let greyLight = UIColor(hexColor: .greyLight)
    static let red = UIColor(hexColor: .red)
    static let silver = UIColor(hexColor: .silver)
    static let yellow = UIColor(hexColor: .yellow)
    static let yellowDark = UIColor(hexColor: .yellowDark)
    static let blueDefaultIOS = UIColor(hexColor: .blueDefaultIOS)
    static let blueDark = UIColor(hexColor: .blueDark)
    static let blue = UIColor(hexColor: .blue)
    static let greenAlpha = UIColor(hexColor: .greenAlpha)
    static let redAlpha = UIColor(hexColor: .redAlpha)
    
    
    convenience init(hexColor: HexColor) {
        self.init(hex6: hexColor.rawValue)
    }
    
    
}
