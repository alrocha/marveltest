
# FARFETCH TEST CODE #

### Architecture ###

* The app has been done with clean architecture following the Clean Swift guide
* In my opinion is a really good architecture (easy to understand after a couple of scenes done) for big projects with big teams, is quite easy to integrate a new person and update them in the project in a couple of weeks. 
[Clean Swift](https://clean-swift.com/)


### Things to improve ###

* Many things can be improved in the UI  and probably take out some functions from the view controller to a protocol to be reusable in the entire app
* Testing
* Error handling. I haven’t add every case for the error codes in the app (lack of time probably) but the MARVEL API doesn’t fail at all :D 


### Notes ###

* Regarding the favourites I have used UserDefaults because in this case is a tiny thing to store per session that doesn’t make sense to create a model and store it in core data. 
* As the network calls are done manually I have had to force in the interactor a main thread change call, as the calls are doing async, and everything related the UI must be in the main thread this will cover any thread change to the main one. 

### Testing ###

* I do not have as much experience with tests, is already a big deal to know the entire iOS SDK and architecture patterns so, step by step I am getting there asap.